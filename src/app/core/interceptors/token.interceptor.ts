import { environment } from './../../../environments/environment';
import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.outsideApiRequest(request)) {
      return next.handle(request);
    }
    if (!this.authService.getToken()) {
      return next.handle(request);
    }

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.authService.getToken()}`
      }
    });
    return next.handle(request);
  }

  private outsideApiRequest(request: HttpRequest<any>) {
    if (request.url.startsWith(environment.fnwApiUrl)) {
      return false;
    }
    return true;
  }
}
