export class Airport {
  id: number;
  airport_name: string;
  iata_code: string;
  country_name: string;
  city_name: string;
  display: string;
}
