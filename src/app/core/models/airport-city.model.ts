export class AirportCity {
  iata_code: number;
  city_id: number;
  city_name: string;
}
