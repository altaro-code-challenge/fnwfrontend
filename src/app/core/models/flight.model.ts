import { Moment } from 'moment';

export class Flight {
          type: string; // onward or return
          airline: string;
          departureDate: Moment;
          arrivalDate: Moment;
          stopsNo: number;
          flightNo: string;
          departureIATA: string;
          arrivalIATA: string;
          sourceWeatherData: any;
          destinationWeatherData: any;

          Equals(otherFlight: Flight): boolean {
            if (otherFlight === undefined || otherFlight === null) { return false; }
            // The flight number should be unique, so I guess it is enough to only check that one.
            if (otherFlight.flightNo === this.flightNo) { return true; }

            return false;
          }
       }
