import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '@env/environment';
import { Subject, Observable, of } from 'rxjs';
import { UserRegister } from '../models/user-register.model';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  // token: string;
  private registerStatus = new Subject<any>();
  registerUrl: string;
  constructor(private httpClient: HttpClient) {}

  register(registerValues: UserRegister): any {
    this.registerUrl = environment.fnwApiUrl + 'users/register';
    return this.httpClient.post<UserRegister>(
      this.registerUrl,
      registerValues,
      { observe: 'response' }
    ).pipe(
      catchError(error => of(error)),
      tap(response => this.setResponse(response))
    );
  }

  setResponse(response: HttpResponse<any>) {
    this.registerStatus.next(response);
  }

  getResponse(): Observable<HttpResponse<any>> {
    return this.registerStatus.asObservable();
  }
}
