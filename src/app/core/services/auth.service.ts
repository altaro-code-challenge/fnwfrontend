import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { of, Observable, throwError, Subject } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { User } from '../../core/models/user.model';
import { environment } from '@env/environment';
import { tap, catchError, map, share } from 'rxjs/operators';
import { JwtHelper } from 'angular2-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authStatus = new Subject<any>();
  authUrl: string;
  checkUrl: string;

  constructor(private httpClient: HttpClient, public jwtHelper: JwtHelper) {}

  login(credentials: User): Observable<HttpResponse<any>> {
    const serverUrl = environment.fnwApiUrl;
    this.authUrl = serverUrl + 'token';
    return this.httpClient
      .post<any>(this.authUrl, credentials, { observe: 'response' })
      .pipe(
        catchError(error => of(error)),
        tap(response => this.setResponse(response))
      );
  }

  public checkToken(): Observable<HttpResponse<any>> {
    const serverUrl = environment.fnwApiUrl;
    this.checkUrl = serverUrl + 'check';
    return this.httpClient
    .get(this.checkUrl, { observe: 'response' })
    .pipe(
      catchError(error => of(error))
    );
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    if (!token) { return false; }
    return !this.jwtHelper.isTokenExpired(token);
  }

  setResponse(response: HttpResponse<any>) {
    this.authStatus.next(response);
  }

  getResponse(): Observable<HttpResponse<any>> {
    return this.authStatus.asObservable();
  }

  getUserLogged(): string {
    const token = this.getToken();
    if (!token) { return ''; }
    const decoded = this.jwtHelper.decodeToken(token);
    return decoded.given_name;
  }

  logout() {
    localStorage.removeItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  setToken(newToken: any) {
    localStorage.setItem('token', newToken);
  }
}
