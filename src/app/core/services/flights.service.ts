import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '@env/environment';
import { Subject, Observable, of } from 'rxjs';
import { UserRegister } from '../models/user-register.model';
import { tap, catchError } from 'rxjs/operators';
import { Airport } from '../models/airport.model';
import { Flight } from '../models/flight.model';
import { AirportCity } from '../models/airport-city.model';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {
  constructor(private httpClient: HttpClient) {}
  private airportsUrl: string;
  private flightsUrl: string;
  private airportsCitiesUrl: string;

  // retrieve airports from our API
  getAirports(search: string): any {
    this.airportsUrl = environment.fnwApiUrl + 'airports/?search=' + search;
    return this.httpClient.get<Airport[]>(this.airportsUrl);
  }

  // retrieve flights data from goibobo API
  getFlights(searchString: string): any {
    this.flightsUrl = environment.goiboboApiUrl + searchString;
    return this.httpClient
      .get<any>(this.flightsUrl)
      .pipe(catchError(error => of(error)));
  }

  getWeatherData(cityId) {
    if (cityId) {
      const weatherUrl = environment.openWeatherMapApiUrl + '&id=' + cityId;
      return this.httpClient
        .get<any>(weatherUrl)
        .pipe(catchError(error => of(error)));
    } else {
      return of(false);
    }
  }

  // Retrieve weather information:
  // - query our API for information about the city ids, based on the airports' iata codes
  // - based on the result construct a query and retrieve weather data from openweathermap's API
  getWeatherCityIds(departureIATA: string, destinationIATA: string): any {
    // TODO - write the backend that will return 2 values: departure + arrival
    const searchString =
      'departurecode=' + departureIATA + '&arrivalcode=' + destinationIATA;
    this.airportsCitiesUrl =
      environment.fnwApiUrl + 'airportcities/?' + searchString;
    return this.httpClient.get<AirportCity[]>(this.airportsCitiesUrl);
  }
}
