import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { environment } from '@env/environment';
import { AuthGuard } from './guards/auth.guard';
import { NoAuthGuard } from './guards/no-auth.guard';

import { NgxSpinnerModule } from 'ngx-spinner';
import { throwIfAlreadyLoaded } from './guards/module-import.guard';
import { JwtHelper } from 'angular2-jwt';

@NgModule({
    imports: [
        HttpClientModule,
        NgxSpinnerModule
    ],
    providers: [
        AuthGuard,
        NoAuthGuard,
        JwtHelper
    ]
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
