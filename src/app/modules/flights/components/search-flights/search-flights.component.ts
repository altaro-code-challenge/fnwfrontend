import { ValidationService } from './../../../../shared/services/validation.service';
import { FlightsService } from './../../../../core/services/flights.service';
import { Component, OnInit, SimpleChanges, Input, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl  } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { Airport } from '@app/core/models/airport.model';
import { map } from 'rxjs/operators';
import moment = require('moment');
import { NgSelectConfig } from '@ng-select/ng-select';

@Component({
  selector: 'app-search-flights',
  templateUrl: './search-flights.component.html',
  styleUrls: ['./search-flights.component.scss']
})
export class SearchFlightsComponent implements OnInit {
  @Output() searchFlightsSubmit = new EventEmitter();
  @Input() isLoading: boolean;
  searchFlightsForm: FormGroup;
  sourceAirportPlaceholder: string;
  destinationAirportPlaceholder: string;
  sourceAirports$: Observable<Airport>;
  destinationAirports$: Observable<Airport>;
  previousSourceSearch: string;
  previousDestinationSearch: string;
  searchString: string;


  constructor(
    private flightsService: FlightsService,
    private formBuilder: FormBuilder
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this.previousSourceSearch = '';
    this.previousDestinationSearch = '';
    this.sourceAirportPlaceholder =
      'Source airport';
    this.destinationAirportPlaceholder =
      'Destination airport';
  }

  get controls() {
    return this.searchFlightsForm.controls;
  }

  onSourceSearchChange(value: string) {
    if (value.length > 0 && this.previousSourceSearch === '') {
      this.sourceAirports$ = this.flightsService.getAirports(value).pipe(
        map((airports: Airport[]) => {
          airports.forEach(airport => {
            airport.display =
              airport.airport_name +
              ' - ' +
              airport.iata_code +
              ' - ' +
              airport.city_name +
              ' - ' +
              airport.country_name;
          });
          return airports;
        })
      );
    }
    if (value.length === 0) {
      this.sourceAirports$ = undefined;
    }
    this.previousSourceSearch = value;
  }

  onDestinationSearchChange(value: string) {
    if (value.length > 0 && this.previousDestinationSearch === '') {
      this.destinationAirports$ = this.flightsService.getAirports(value).pipe(
        map((airports: Airport[]) => {
          airports.forEach(airport => {
            airport.display =
              airport.airport_name +
              ' - ' +
              airport.iata_code +
              ' - ' +
              airport.city_name +
              ' - ' +
              airport.country_name;
          });
          return airports;
        })
      );
    }
    if (value.length === 0) {
      this.destinationAirports$ = undefined;
    }
    this.previousDestinationSearch = value;
  }

  selectClasses(control: FormControl) {
    return {
      'is-valid': control.touched && control.valid,
      'is-invalid': control.touched && !control.valid
    };
  }

  inputClasses(control: FormControl) {
    return {
      'form-control': true,
      'is-valid': control.touched && control.valid,
      'is-invalid': control.touched && !control.valid
    };
  }

  private buildForm(): void {
    this.searchFlightsForm = this.formBuilder.group({
      sourceAirport: [
        null,
        [
          Validators.required,
          ValidationService.differentThanDestinationValidator
        ]
      ],
      destinationAirport: [
        null,
        [
          Validators.required,
          ValidationService.differentThanSourceValidator
        ]
      ],
      departureDatePicker: [
        '',
        [
          Validators.required,
          ValidationService.dateNotPassedValidator,
          ValidationService.departureAfterArrivalValidator
        ]
      ],
      arrivalDatePicker: [
        '',
        [
          Validators.required,
          ValidationService.dateNotPassedValidator,
          ValidationService.arrivalBeforeDepartureValidator
        ]
      ],
      adultsNo: [
        1,
        [Validators.min(1), Validators.max(9), Validators.required]
      ],
      childrenNo: [
        0,
        [Validators.min(0), Validators.max(9), Validators.required]
      ]
    });
  }

  searchFlights() {
    this.searchFlightsSubmit.emit(this.searchFlightsForm.controls);
  }
}
