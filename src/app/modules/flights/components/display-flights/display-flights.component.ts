import { Component, OnInit, Input } from '@angular/core';
import { Flight } from '@app/core/models/flight.model';

@Component({
  selector: 'app-display-flights',
  templateUrl: './display-flights.component.html',
  styleUrls: ['./display-flights.component.scss']
})
export class DisplayFlightsComponent implements OnInit {
  @Input() flightsList: Flight[];
  @Input() isLoading: boolean;

  constructor() {}

  ngOnInit() {}

  showTemperature(weatherData: any) {
    if (weatherData === undefined) { return; }
    return Math.round(weatherData.main.temp - 273.15);
  }

  loadWeatherStyles(code: string) {
    let styles = {};
    switch (code) {
      case '800':
        styles = { 'background-color': 'yellow' };
        break;
      case '800':
        styles = { 'background-color': 'yellow' };
        break;
      case '800':
        styles = { 'background-color': 'yellow' };
        break;

      default:
        styles = { 'background-color': 'white' };
        break;
    }
    return styles;
  }
}
