import { FlightsRoutingModule } from './flights-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightsComponent } from './pages/flights/flights.component';
import { SearchFlightsComponent } from './components/search-flights/search-flights.component';
import { DisplayFlightsComponent } from './components/display-flights/display-flights.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared/shared.module';
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    FlightsComponent,
    SearchFlightsComponent,
    DisplayFlightsComponent
  ],
  imports: [
    CommonModule,
    FlightsRoutingModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    DlDateTimePickerDateModule,
    MomentModule
  ]
})
export class FlightsModule {}
