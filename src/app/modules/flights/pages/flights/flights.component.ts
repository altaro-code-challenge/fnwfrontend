import { FlightsService } from './../../../../core/services/flights.service';
import { Component, OnInit } from '@angular/core';
import { Flight } from '@app/core/models/flight.model';
import { finalize, map, concatMap, mergeMap, tap } from 'rxjs/operators';
import { AirportCity } from '@app/core/models/airport-city.model';
import { FormGroup, AbstractControl } from '@angular/forms';
import moment = require('moment');

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {
  controls: AbstractControl;
  alertMessage: string;
  alertType: string;
  flightsList: Flight[];
  toggleSearch: boolean;
  isLoading: boolean;
  departureAirportCity: AirportCity;
  arrivalAirportCity: AirportCity;
  sourceCityId: number;
  destinationCityId: number;
  sourceWeatherData: any;
  destinationWeatherData: any;
  isSourceWeatherDataAvailable: boolean;
  isDestinationWeatherDataAvailable: boolean;

  constructor(private flightsService: FlightsService) {}

  ngOnInit() {
    this.isLoading = false;
    this.toggleSearch = false;
    this.alertType = 'info';
    this.alertMessage = '';
  }

  private constructSearchString(formControls: AbstractControl) {
    // &source=SKP&destination=CRL&dateofdeparture=20181214&dateofarrival=20181221&seatingclass=E&adults=1&children=0&infants=0&counter=100
    this.controls = formControls;
    const returnString =
      '&source=' +
      this.controls['sourceAirport'].value +
      '&destination=' +
      this.controls['destinationAirport'].value +
      '&dateofdeparture=' +
      moment(this.controls['departureDatePicker'].value).format('YYYYMMDD') +
      '&dateofarrival=' +
      moment(this.controls['arrivalDatePicker'].value).format('YYYYMMDD') +
      '&adults=' +
      this.controls['adultsNo'].value +
      '&children=' +
      this.controls['childrenNo'].value;

    return returnString;
  }

  displaySearchResults(formControls: AbstractControl) {
    const searchString = this.constructSearchString(formControls);
    this.isLoading = true;

    this.flightsService
      .getFlights(searchString)
      .pipe(
        tap(response => this.mapTheFlights(response)),
        mergeMap(response =>
          this.flightsService.getWeatherCityIds(
            this.controls['sourceAirport'].value,
            this.controls['destinationAirport'].value
          )
        ),
        tap((response: AirportCity[]) => this.mapWeatherCityIds(response)),
        concatMap(() => this.flightsService.getWeatherData(this.sourceCityId)),
        tap(response => this.mapWeatherData(response, 'source')),
        mergeMap(() =>
          this.flightsService.getWeatherData(this.destinationCityId)
        ),
        map(response => this.mapWeatherData(response, 'destination')),
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe();
  }

  mapWeatherCityIds(airportCities: AirportCity[]) {
    airportCities.forEach(airportCity => {
      if (this.controls['sourceAirport'].value === airportCity.iata_code) {
        this.sourceCityId = airportCity.city_id;
      }
      if (this.controls['destinationAirport'].value === airportCity.iata_code) {
        this.destinationCityId = airportCity.city_id;
      }
    });
    if (this.sourceCityId) { this.isSourceWeatherDataAvailable = false; }
    if (this.sourceCityId) { this.isDestinationWeatherDataAvailable = false; }
  }

  mapWeatherData(response: any, type: string) {
    if (response) {
      if (type === 'source') {
        this.populateWeatherData('source', response.list);
      }
      if (type === 'destination') {
        this.populateWeatherData('destination', response.list);
      }
    }
  }

  populateWeatherData(type: string, weatherDataDTOs: any[]) {
    let previousWeatherDataDTO: any;
    this.flightsList.forEach(flight => {
      weatherDataDTOs.forEach(weatherDataDTO => {
        if (previousWeatherDataDTO === undefined) {
          previousWeatherDataDTO = weatherDataDTO;
        }
        if (type === 'source') {
          if (
            flight.departureDate.isBetween(
              moment(previousWeatherDataDTO.dt_txt),
              moment(weatherDataDTO.dt_txt)
            )
          ) {
            flight.sourceWeatherData = previousWeatherDataDTO;
          } else {
          }
        }
        if (type === 'destination') {
          if (
            flight.arrivalDate.isBetween(
              moment(previousWeatherDataDTO.dt_txt),
              moment(weatherDataDTO.dt_txt)
            )
          ) {
            flight.destinationWeatherData = previousWeatherDataDTO;
          }
        }
        previousWeatherDataDTO = weatherDataDTO;
      });
    });
  }

  private mapTheFlights(response: any) {
    const data = response.data;
    if (data) {
      if (data.Error) {
        this.alertMessage = 'Couldn\'t retrieve information from the server:\n';
        this.alertMessage += response.Error;
        this.alertType = 'danger';
      } else {
        if (data.onwardflights) {
          this.flightsList = new Array<Flight>();
          data.onwardflights.forEach(flightDTO => {
            const flight = this.newFlight(flightDTO, 'onward');
            // there were duplicates in the json when I was testing, so I am filtering them out here
            if (
              !(
                this.flightsList.filter(_flight => _flight.Equals(flight))
                  .length > 0
              )
            ) {
              this.flightsList.push(flight);
            }
          });
        }
        if (data.returnflights) {
          data.returnflights.forEach(flightDTO => {
            const flight = this.newFlight(flightDTO, 'return');
            this.flightsList.push(flight);
          });
        }
        this.toggleSearch = true;
      }
    } else {
      this.alertMessage = 'Error connecting to the server\n';
      this.alertMessage += response.error;
      this.alertType = 'danger';
    }
  }

  private newFlight(flightDTO: any, type: string) {
    const flight = new Flight();
    flight.type = type;
    (flight.airline = flightDTO.airline),
      (flight.departureDate = moment(flightDTO.depdate, 'YYYY-MM-DDtHH:SS')),
      (flight.arrivalDate = moment(flightDTO.arrdate, 'YYYY-MM-DDtHH:SS')),
      (flight.stopsNo = flightDTO.stops),
      (flight.flightNo = flightDTO.operatingcarrier + flightDTO.flightno);

    return flight;
  }
}
