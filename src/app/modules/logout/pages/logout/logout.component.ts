import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  @Input() name: string;

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }


  logout() {
    this.authService.logout();
    this.router.navigate(['auth/login']);
  }

}
