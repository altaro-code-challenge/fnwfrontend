import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap, delay, finalize, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '@app/core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error: string;
  isLoading: boolean;
  loginForm: FormGroup;
  registerPath = '/auth/register';
  flightsPath = '/flights';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    this.buildForm();
  }

  ngOnInit() { }

  get controls() {
    return this.loginForm.controls;
  }

  login() {
    this.isLoading = true;
    const credentials = this.loginForm.value;
    this.authService
      .login(credentials)
      .pipe(
        finalize(() => this.isLoading = false),
        catchError(error => of(this.error = error)),
      ).subscribe(response => this.handleResponse(response));
  }

  private async handleResponse(response: any) {
    if (response instanceof HttpResponse) {
      const newToken: string = response.body;
      this.authService.setToken(newToken);
      this.router.navigate(['flights']);
    }
  }

  private buildForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

}
