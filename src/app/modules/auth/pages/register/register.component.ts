import { environment } from '@env/environment';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { tap, finalize, catchError, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { RegisterService } from '@app/core/services/register.service';
import { ValidationService } from '@app/shared/services/validation.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  error: string;
  isLoading: boolean;
  loginPath = '/auth/login';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registerService: RegisterService
  ) {
    this.buildForm();
  }

  ngOnInit() {}

  get controls() {
    return this.registerForm.controls;
  }

  register() {
    this.isLoading = true;
    const registerValues = this.registerForm.value;
     this.registerService
        .register(registerValues)
         .pipe(
           finalize(() => this.isLoading = false),
           catchError(error => of(error))
         ).subscribe(response => this.handleResponse(response));
  }

  getClasses(control: FormControl) {
    return {
      'form-control': true,
      'is-valid': control.touched && control.valid,
      'is-invalid': control.touched && !control.valid
    };
  }

  private handleResponse(response: HttpResponse<any>) {
    if (response instanceof HttpResponse) {
      this.router.navigate(['auth/login']);
    }
  }

  private buildForm(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', [ValidationService.emailValidator, Validators.required]],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm: ['', [Validators.required, ValidationService.matchPasswordValidator]]
    });
  }
}
