import { NotfoundModule } from './modules/notfound/notfound.module';
import { NoAuthGuard } from './core/guards/no-auth.guard';
import { LogoutModule } from './modules/logout/logout.module';
import { AboutModule } from './modules/about/about.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { ContentLayoutComponent } from './layouts/content-layout/content-layout.component';
import { SharedModule } from './shared/shared.module';
import { JwtHelper } from 'angular2-jwt';
import { AuthGuard } from './core/guards/auth.guard';
import { TokenInterceptor } from './core/interceptors/token.interceptor';
import { FlightsModule } from './modules/flights/flights.module';
import { LayoutModule } from './shared/layout/layout.module';


@NgModule({
  declarations: [AppComponent, AuthLayoutComponent, ContentLayoutComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    FlightsModule,
    AboutModule,
    LayoutModule,
    LogoutModule,
    NotfoundModule
  ],
  providers: [
    JwtHelper,
    AuthGuard,
    NoAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
