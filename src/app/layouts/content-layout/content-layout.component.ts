import { AuthService } from '@app/core/services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-layout',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.scss']
})
export class ContentLayoutComponent implements OnInit {

  userLogged: string;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {

    this.userLogged = this.authService.getUserLogged();
  }

}
