import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '@app/core/services/auth.service';
import { RegisterService } from '@app/core/services/register.service';
import { delay } from 'q';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss']
})
export class AuthLayoutComponent implements OnDestroy {
  private subscription: Subscription;
  alertType: string;
  alertMessage: string;

  constructor(
    private registerService: RegisterService,
    private authService: AuthService
  ) {
    this.subscription = this.registerService
      .getResponse()
      .subscribe(response => {
        this.setRegisterAlertMsg(response);
      })
      .add(
        this.authService.getResponse().subscribe(response => {
          this.setAuthAlertMsg(response);
        })
      );

    // this.subscription.add(
    //   this.authService.getResponse().subscribe(response => {
    //     this.setAlert(response);
    //   })
    // );
  }

  updateMessage(message: string) {
    this.alertMessage = message;
  }

  setRegisterAlertMsg(response: any) {
    if (response instanceof HttpResponse) {
      switch (response.status) {
        case 201:
          this.alertType = 'success';
          this.alertMessage = `The user ${response.body['username']} has been registered successfully.\n`;
          this.alertMessage += 'You can now login with your username and password';
          break;
        case 202:
          this.alertType = 'warning';
          this.alertMessage = 'The user has been registered but the information could not be verified.\n';
          this.alertMessage += 'You can try loging in with your username and password\n';
          this.alertMessage += 'If you have any trouble, please contact the administrator.';
          break;
        default:
          this.alertType = 'success';
          this.alertMessage = 'The user has been registered successfully.\n';
          this.alertMessage += 'You can now login with your username and password';
          break;
      }
    } else if (response instanceof HttpErrorResponse) {
      this.alertType = 'danger';
      switch (response.status) {
        case 400:
          this.alertMessage = 'The user could not be registered. Please fix the errors below and try again:\n';
          if (response.error !== 'undefined') {
            this.alertMessage += response.error;
          }
          break;
        case 500:
          this.alertMessage = 'There was a problem with the server.\n';
          this.alertMessage += 'You can try again, and if the problem persists, please contact the administrator.\n';
          break;
        default:
          this.alertMessage = 'An error occured while trying to register your account.\n';
          this.alertMessage += 'You can try again, and if the problem persists, please contact the administrator.';
          break;
      }
    }
  }

  setAuthAlertMsg(response: any) {
    if (response instanceof HttpResponse) {
      this.alertType = 'success';
      switch (response.status) {
        case 200:
          this.alertMessage = `Login successful`;
          break;
        default:
          this.alertMessage = `Login successful`;
          break;
      }
    } else if (response instanceof HttpErrorResponse) {
             this.alertType = 'danger';
             switch (response.status) {
               case 401:
                 this.alertMessage = 'Login failed:\n';
                 if (response.error !== 'undefined') {
                   this.alertMessage += response.error;
                 }
                 break;
               case 403:
                 this.alertMessage = 'Unauthorized access:\n';
                 if (response.error !== 'undefined') {
                   this.alertMessage += response.error;
                 }
                 break;
               case 500:
                 this.alertMessage = 'There was a problem with the server.\n';
                 this.alertMessage += 'You can try again, and if the problem persists, please contact the administrator.\n';
                 break;
               default:
                 this.alertMessage = 'An error occured while trying to login to your account.\n';
                 this.alertMessage += 'You can try again, and if the problem persists, please contact the administrator.';
                 break;
             }
           }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
