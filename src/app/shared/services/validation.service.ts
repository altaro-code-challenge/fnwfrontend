import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, FormControl } from '@angular/forms';
import moment = require('moment');

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  public static getValidationErrorMessage(validatorName: string, validatorValue?: any, labelName?: string): any {
    const config = {
      required: `Field is required.`,
      invalidEmail: `The email is not in a correct format.`,
      invalidPassword: 'Invalid password. Password must be at least 6 characters long, and contain a number.',
      passwordsNotMatching: 'The password confirmation does not match.',
      maxlength: `The field can't contain more than ${validatorValue.requiredLength} characters.`,
      minlength: `The field must contain atleast ${validatorValue.requiredLength} characters.`,
      dateInPast: `The date has to be in the future`,
      arrivalBeforeDeparture: `The arrival date cannot be before the departure date`,
      departureAfterArrival: `The departure date cannot be after the arrival date`,
      differentThanSourceAirport: `The destination airport cannot be the same as the source`,
      differentThanDestinationAirport: `The destination airport cannot be the same as the source`,
    };

    return config[validatorName];
  }

  /**** Registration form validators ****/
  public static passwordValidator(control: AbstractControl): any {
    if (!control.value) { return; }

    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    // (?!.*\s)          - Spaces are not allowed
    return (control.value.match(/^(?=.*\d)(?=.*[a-zA-Z!@#$%^&*])(?!.*\s).{6,100}$/))
      ? '' :
      { invalidPassword: true };
  }

  public static matchPasswordValidator(control: FormControl): any {
    if (!control.value) { return; }
    const otherControl = control.parent.get('password');
    return (control.value === otherControl.value) ? '' : { passwordsNotMatching: true };
  }

  public static emailValidator(control: AbstractControl): any {
    if (!control.value) { return; }
    // RFC 2822 compliant regex
    // tslint:disable-next-line:max-line-length
    return control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)
                                ? ''
                                : { invalidEmail: true };
  }

    /**** Search flights validators ****/
  public static dateNotPassedValidator(control: FormControl): any {
    if (!control.value) { return; }
    return moment().diff(control.value) < 0 ? '' : { dateInPast: true };
  }

  public static arrivalBeforeDepartureValidator(control: FormControl): any {
    if (!control.value) { return; }
    const otherControl = control.parent.get('departureDatePicker');
    if (!otherControl.value) { return; }
    return (moment(control.value) >= moment(otherControl.value)) ? '' : { arrivalBeforeDeparture: true };
  }

  public static departureAfterArrivalValidator(control: FormControl): any {
    if (!control.value) { return; }
    const otherControl = control.parent.get('arrivalDatePicker');
    if (!otherControl.value) { return; }
    return (moment(control.value) < moment(otherControl.value)) ? '' : { departureAfterArrival: true };
  }

  public static differentThanDestinationValidator(control: FormControl): any {
    if (!control.value) { return; }
    const otherControl = control.parent.get('destinationAirport');
    if (!otherControl.value) { return; }
    return control.value !== otherControl.value ? '' : { differentThanDestinationAirport: true };
  }

  public static differentThanSourceValidator(control: FormControl): any {
    if (!control.value) { return; }
    const otherControl = control.parent.get('sourceAirport');
    if (!otherControl.value) { return; }
    return control.value !== otherControl.value ? '' : { differentThanSourceAirport: true };
  }

}
