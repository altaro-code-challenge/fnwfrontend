import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LayoutModule } from './layout/layout.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ValidationLabelComponent } from './components/validation-label/validation-label.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ValidationLabelGroupComponent } from './components/validation-label-group/validation-label-group.component';
import { AlertComponent } from './components/alert/alert.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LayoutModule,
    NgbModule.forRoot()
  ],
  declarations: [
    ValidationLabelComponent,
    SpinnerComponent,
    ValidationLabelGroupComponent,
    AlertComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LayoutModule,
    NgbModule,
    ValidationLabelComponent,
    ValidationLabelGroupComponent,
    SpinnerComponent,
    AlertComponent,
  ]
})
export class SharedModule {}
