import { Routes } from '@angular/router';
import { AboutComponent } from '@app/modules/about/pages/about/about.component';
import { FlightsComponent } from '@app/modules/flights/pages/flights/flights.component';
import { LogoutComponent } from '@app/modules/logout/pages/logout/logout.component';

export const CONTENT_ROUTES: Routes = [
  { path: 'flights', component: FlightsComponent, loadChildren: './modules/flights/flights.module#FlightsModule' },
  { path: 'about', component: AboutComponent, loadChildren: './modules/about/about.module#AboutModule' },
  { path: 'logout', component: LogoutComponent, loadChildren: './modules/logout/logout.module#LogoutModule' }
];
