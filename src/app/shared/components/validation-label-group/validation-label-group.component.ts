import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from '@app/shared/services/validation.service';

@Component({
  selector: 'app-validation-label-group',
  templateUrl: './validation-label-group.component.html',
  styleUrls: ['./validation-label-group.component.scss']
})
export class ValidationLabelGroupComponent {
  @Input()
  public controlGroup: FormGroup;
  @Input()
  public control: FormControl;
  @Input()
  public labelName?: string;

  constructor() { }

  get errorMessage(): boolean {
    for (const propertyName in this.controlGroup.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidationErrorMessage(propertyName, this.controlGroup.errors[propertyName], this.labelName);
      }
    }
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidationErrorMessage(propertyName, this.control.errors[propertyName], this.labelName);
      }
    }
    return undefined;
  }

}
