import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationLabelGroupComponent } from './validation-label-group.component';

describe('ValidationLabelGroupComponent', () => {
  let component: ValidationLabelGroupComponent;
  let fixture: ComponentFixture<ValidationLabelGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationLabelGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationLabelGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
