import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input() public type: string;
  @Input() public message: string;
  @Input() public show: boolean;
  @Output() messageChanged = new EventEmitter();

  async close_clicked() {
    this.message = '';
    this.messageChanged.emit(this.message);
  }

  public ngOnInit(): void {
    this.type = 'info';
    this.message = '';
  }

  constructor() {}
}
