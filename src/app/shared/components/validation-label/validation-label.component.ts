import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationService } from '../../services/validation.service';

@Component({
  selector: 'app-validation-label',
  templateUrl: './validation-label.component.html',
  styleUrls: ['./validation-label.component.scss']
})
export class ValidationLabelComponent {
  @Input()
  public control: FormControl;
  @Input()
  public labelName?: string;

  constructor() {}

  get errorMessage(): boolean {
    for (const propertyName in this.control.errors) {
      if (
        this.control.errors.hasOwnProperty(propertyName) &&
        this.control.touched
      ) {
        return ValidationService.getValidationErrorMessage(
          propertyName,
          this.control.errors[propertyName],
          this.labelName
        );
      }
    }
    return undefined;
  }
}
